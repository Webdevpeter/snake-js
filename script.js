var screen, matrix, frames, lvFrame;
var snake, food;
var dirX, dirY;
var gameWidth, gameHeight, lastPressed;
var score = document.getElementById('score');

function init() {
	frames = 0;
	lvFrame = 5;
	gameWidth = 20;
	gameHeight = 20;
	dirX = 1;
	dirY = 0;

	document.addEventListener("keydown", function(ev) {
		var keyCode = ev.which || ev.keyCode;
		if(keyCode === 37) { // Arrow left
			lastPressed = 37;
		}
		if(keyCode === 38) { // Arrow up
			lastPressed = 38;
		}
		if(keyCode === 39) { // Arrow right
			lastPressed = 39;
		}
		if(keyCode === 40) { // Arrow down
			lastPressed = 40;
		}
	});

	snake = {
		alive: true,
		score: 0,
		body: [
			{
				x: Math.floor(gameWidth/2),
				y: Math.floor(gameHeight/2)
			},
			{
				x: Math.floor(gameWidth/2)-1,
				y: Math.floor(gameHeight/2)
			},
			{
				x: Math.floor(gameWidth/2)-2,
				y: Math.floor(gameHeight/2)
			},
			{
				x: Math.floor(gameWidth/2)-3,
				y: Math.floor(gameHeight/2)
			},
			{
				x: Math.floor(gameWidth/2)-4,
				y: Math.floor(gameHeight/2)
			}
		]
	};
	score.innerHTML = snake.score;
	generateFood();
}

function main() {
	screen = new Screen(400, 400);
	screen.ctx.scale(20, 20);

	init();
	run();
}

function printBody() {
	screen.ctx.save();
	screen.ctx.fillStyle = '#000';
	snake.body.forEach(function(part) {
		screen.ctx.fillRect(part.x, part.y, 1, 1);
	})
	screen.ctx.restore();
}

function printFood() {
	screen.ctx.save();
	screen.ctx.fillStyle = 'red';
	screen.ctx.fillRect(food.x, food.y, 1, 1);
	screen.ctx.restore();
}

function render() {
	screen.clear();
	screen.ctx.fillStyle = '#fff';
	screen.ctx.fillRect(0, 0, screen.canvas.width, screen.canvas.height);
	printBody();
	printFood();
}

function run() {
	var loop = function(){
		update();
		render();
		if(snake.alive) {
			requestAnimationFrame(loop, screen.canvas);
		} else {
			if(confirm('Game Over. Play again?')) {
				init();
				run();
			}
		}
	}
	requestAnimationFrame(loop, screen.canvas);
}

function generateFood() {
	food = {
		x: Math.floor(Math.random() * (gameWidth-1)) + 1,
		y: Math.floor(Math.random() * (gameHeight-1)) + 1
	}
}

function snakeStep() {
	snake.body.unshift({
		x: (snake.body[0].x + dirX < 0) ? gameWidth - 1 : (snake.body[0].x + dirX > gameWidth - 1) ? 0 : snake.body[0].x + dirX,
		y: (snake.body[0].y + dirY < 0) ? gameHeight - 1 : (snake.body[0].y + dirY > gameHeight - 1) ? 0 : snake.body[0].y + dirY
	});
	snake.body.pop();
}

function update() {
	if(snake.body.length > 10) {
		lvFrame = 4;
	}
	if(snake.body.length > 20) {
		lvFrame = 3;
	}
	if(snake.body.length > 30) {
		lvFrame = 2;
	}

	
	frames++;
	if(frames % lvFrame === 0) {

		if(lastPressed === 37 && dirX !== 1) { // Arrow left
			dirX = -1;
			dirY = 0;
		}
		if(lastPressed === 38 && dirY !== 1){ // Arow up
			dirX = 0;
			dirY = -1;
		}
		if(lastPressed === 39 && dirX !== -1) { // Arrow right
			dirX = 1;
			dirY = 0;
		}
		if(lastPressed === 40 && dirY !== -1) {	// Arrow down
			dirX = 0;
			dirY = 1;
		}

		snakeStep();
		var snakeLen = snake.body.length;
		if(AABBIntersect(snake.body[0].x, snake.body[0].y, food.x, food.y)) {
			generateFood();
			snake.body.push(snake.body[snakeLen-1]);
			if(snake.body.length < 10) {
				snake.score += 10;
			} else if(snake.body.length < 20) {
				snake.score += 20;
			} else if(snake.body.length < 50) {
				snake.score += 30;
			} else {
				snake.score += 100;
			}
			score.innerHTML = snake.score;
		}

		if(snakeLen > 4) {
			snake.body.forEach(function(part, i){
				if(i !== 0 && AABBIntersect(snake.body[0].x, snake.body[0].y, part.x, part.y)) {
					snake.alive = false;
				}
			});
		}
	}
}

main();