// Snake helpers


// Screen
function Screen(w, h) {
	this.canvas = document.createElement("canvas");
	this.canvas.width = this.width = w;
	this.canvas.height = this.height = h;
	this.ctx = this.canvas.getContext("2d");

	document.body.appendChild(this.canvas);
}

Screen.prototype.clear = function() {
	this.ctx.clearRect(0, 0, this.width, this.height);
}

// Collision check
function AABBIntersect(x1, y1, x2, y2) {
	return x1 === x2 && y1 === y2;
}